\section{Architecture}

\begin{figure}[hbt]
    \includegraphics[width=\textwidth]{technical-whitepaper/04_architecture/network-diagram.png}
    \caption{Network Diagram}
    \label{fig:network-diagram}
\end{figure}

\subsection{Distributed Ledger/Blockchain}
The Xand Distributed Ledger is a blockchain. Xand uses a blockchain because no other
technology offers the ability to build a decentralized trustless ledger.
There are critical areas of infrastructure where it is important not to "roll your own"
solution.\footnote{You Really Shouldn't Roll Your Own Crypto: An Empirical Study of
Vulnerabilities in Cryptographic Libraries arXiv:2107.04940 \url{https://arxiv.org/abs/2107.04940}}
We believe blockchain consensus, gossiping, chain upgrades and certain aspects
of validation are among them. For this reason, Xand builds on top of Substrate, a well-
vetted, open source, customizable blockchain framework. Substrate provides Xand with the
foundational features needed for a blockchain system while allowing easy implementation
of custom logic.

Most blockchain-based systems are difficult to evolve and upgrade because of their
decentralized nature. Typically, every party needs to upgrade to the latest version of
software at the same time or risk forking the chain. Xand leverages Substrate\footnote{For
more on Transparent's selection of Substrate, see "Why We Built the Xand Network Using
Substrate," Seth Paulson, December 10, 2021; 
\url{https://transparent.us/post/why-we-built-the-xand-network-using-substrate}} to provide
forkless upgrades through an on-chain mechanism that ensures the whole network stays
coordinated.

\subsection{Services}
Each participant runs software to allow it to connect to a Xand network as well as to
facilitate actions on a network appropriate to its role. The software associated with Xand is
broken up into units of functionality called Services. Each participant will deploy some
subset of the services, as shown in \autoref*{tab:services-by-participant}.

\ctable[
    caption=Services Run by Each Participant,label=tab:services-by-participant,
    pos=htb
]{lcccc}{}{
                                                                                                \FL
                    & Xand Service  & Trustee Service   & Member Service    & Voting Service    \ML
    Member          & \checkmark    &                   & \checkmark        & \checkmark        \NN
    Validator       & \checkmark    &                   &                   & \checkmark        \NN
    Trustee         & \checkmark    & \checkmark        &                   &                   \NN
    Limited Agent   & \checkmark    &                   &                   & \checkmark        \NN
    X-E Bank        &               &                   &                   &                   \LL
}

\subsubsection{Xand Service}
The Xand Service provides access to the blockchain. This service is run by all Participants
as a part of their Node. The Xand Service is comprised of a few components:

\begin{itemize}
    \item Substrate Node – This is the component that communicates with the rest of the
    network via gossiping and consensus. It stores a complete copy of the distributed
    ledger in the form of a blockchain. Only nodes run by Validators participate in
    consensus to add new blocks to the chain.
    \item Xand API – This Rust API built by Transparent provides for submitting transactions
    as well as searching transaction history.
    \item Indexing Component – This component built by Transparent stores the ledger
    transactions in a searchable way after decoding confidential transactions
\end{itemize}

\subsubsection{Trustee Service}
The Trustee Service is a fully automated system that observes the ledger for any pending
Create or Redeem requests. On seeing a Create Request, it looks for a matching bank
transfer and if it finds one issues a Cash Confirmation to the ledger. On seeing a Redeem
request, it transfers the specified amount into the specified bank account and then confirms
this action on the ledger. This service is run only by the Trustee. The Trust service is
comprised of three components:

\begin{itemize}
    \item Trust Application – This is a Rust application built by Transparent that automates
    all the Trustee's duties around creation and redemption. It talks to the Xand Service
    and the Bank APIs.
    \item Bank Transaction Repository – This is a Rust library built by Transparent that
    caches transaction history from the banks and ensures an unambiguous (ISO 8601)
    timestamp. This component talks to the bank APIs periodically, harvesting new
    bank transactions.
    \item Bank Transaction Repository DB – This is a SQL database used by the Bank
    Transaction Repository.
    
\end{itemize}

\subsubsection{Member Service}
The Member Service provides a convenient API for Members to write payment automation
with. It makes creating, sending, and redeeming claims very straightforward. This service
is run only by the Member. It is comprised of two components:


\begin{itemize}
    \item Member API – This is a REST API with endpoints for creating, redeeming, and
    sending claims. It talks to the Xand Service and the Bank APIs.
    \item Member Service DB – This is a SQL database that stores bank and account
    information for the Members.
\end{itemize}

\subsubsection{Network Voting Service}
The Network Voting Service provides an interface for creating proposals, viewing existing
proposals with their status, and voting on any open proposals. This service is run by
Members, Validators, and the Limited Agent. It consists of only one component:

\begin{itemize}
    \item Network Voting CLI - A Rust-based command line interface application written by
    Transparent. This component talks to the Xand Service.
\end{itemize}
