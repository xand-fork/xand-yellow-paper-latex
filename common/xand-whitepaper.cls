\ProvidesClass{xand-whitepaper}
\LoadClass[twoside,titlepage]{article}
\usepackage{xkeyval}
\usepackage[margin=1in]{geometry}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{parskip}
\usepackage[medium]{titlesec}
\usepackage[table,xcdraw]{xcolor}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage[title,titletoc]{appendix}
\usepackage{fancyhdr}
\usepackage{enumitem}
\usepackage[section]{placeins}
\usepackage{titling}
\usepackage[english=american, autostyle=false]{csquotes}
\usepackage[newcommands]{ragged2e}
\usepackage[
	labelsep=endash,
	labelfont=bf,]{caption}
\usepackage{etoolbox}

%--------------------------------------
% Branding stuff
%--------------------------------------
\newcommand{\thecompany}{Transparent Financial Systems}
% These logo commands take the same parameters as \includegraphics; you probably want to supply a width
\newcommand{\transparentlogo}[1][]{\includegraphics[#1]{assets/Transparent_logo_w_text_brown_1x.png}}
\newcommand{\xandlogo}[1][]{\includegraphics[#1]{assets/Xand_logo_orange_1x.png}}

%--------------------------------------
% Setup
%--------------------------------------
\definecolor[named]{link-color}{HTML}{4B401F}

\titleclass{\subsubsubsection}{straight}[\subsection]

\newcounter{subsubsubsection}[subsubsection]
\newcommand{\sectionbreak}{\clearpage\thispagestyle{plain}}
\renewcommand\thesubsubsubsection{\thesubsubsection.\arabic{subsubsubsection}}
\renewcommand\theparagraph{\thesubsubsubsection.\arabic{paragraph}} % optional; useful if paragraphs are to be numbered

\hypersetup{
    allcolors=link-color,
    colorlinks=true,
    linktoc=all,
    pdfdisplaydoctitle=true,
    pdfstartview=SinglePage,
    pdfpagemode=UseOutlines,
    bookmarksopen=true,
    bookmarksopenlevel=2,
}
\MakeOuterQuote{"}

\let\Oldsubsection\subsection
\renewcommand{\subsection}{\FloatBarrier\Oldsubsection}

\titlespacing*{\section}{0pt}{3.25ex plus 1ex minus .2ex}{5ex plus .2ex}
\titleformat{\subsubsubsection}{\normalfont\normalsize\bfseries}{\thesubsubsubsection}{1em}{}
\titlespacing*{\subsubsubsection}{0pt}{3.25ex plus 1ex minus .2ex}{1.5ex plus .2ex}

\renewcommand\paragraph{\@startsection{paragraph}{5}{\z@}%
	{3.25ex \@plus1ex \@minus.2ex}%
	{-1em}%
	{\normalfont\normalsize\bfseries}}
\renewcommand\subparagraph{\@startsection{subparagraph}{6}{\parindent}%
	{3.25ex \@plus1ex \@minus .2ex}%
	{-1em}%
	{\normalfont\normalsize\bfseries}}
\def\toclevel@subsubsubsection{4}
\def\toclevel@paragraph{5}
\def\toclevel@paragraph{6}
\def\l@subsubsubsection{\@dottedtocline{4}{7em}{4em}}
\def\l@paragraph{\@dottedtocline{5}{10em}{5em}}
\def\l@subparagraph{\@dottedtocline{6}{14em}{6em}}


\setcounter{secnumdepth}{4}
\setcounter{tocdepth}{3}

\renewcommand\listoffigures{%
    \subsection*{\listfigurename}
      \@mkboth{\MakeUppercase\listfigurename}
              {\MakeUppercase\listfigurename}
    \@starttoc{lof}
}
\renewcommand\listoftables{%
    \subsection*{\listtablename}
      \@mkboth{\MakeUppercase\listtablename}
              {\MakeUppercase\listtablename}
    \@starttoc{lot}
}


\renewcommand{\headrulewidth}{0.4pt}
\renewcommand{\footrulewidth}{0.4pt}

\fancyhead[C]{}
\fancyfoot[RE,LO]{\thecompany}
\fancyfoot[LE,RO]{\@title}

\fancypagestyle{plain}{
	\fancyhf{}
	\fancyfoot[C]{\thepage}
	\renewcommand{\headrulewidth}{0pt}
	\fancyfoot[RE,LO]{\thecompany}
	\fancyfoot[LE,RO]{\@title}
}

\renewcommand{\familydefault}{\sfdefault}
\pagestyle{fancy}

%--------------------------------------
% Title and contents
%--------------------------------------
\newlength{\logospace}
\setlength{\logospace}{230pt}
\newlength{\titlespace}
\setlength{\titlespace}{124pt}

\providecommand{\thesubtitle}{}
\newcommand{\subtitle}[1]{\renewcommand{\thesubtitle}{#1}}
\providecommand{\theabstract}{}
\newcommand{\provideabstract}[1]{\renewcommand{\theabstract}{#1}}

\renewcommand{\maketitle}{
    \thispagestyle{empty}
    \begin{flushright}
        \includegraphics[width=144pt]{assets/Xand_logo_orange_1x.png}
    \end{flushright}
    \vspace*{\logospace}


    \begin{flushleft}
        {\Huge\thetitle}

        \ifdefvoid{\thesubtitle}{{\large\vspace{\baselineskip}}}{{\large\textit{\thesubtitle}}\par}
        
        \lastchangedate
    \end{flushleft}


    \vspace*{\titlespace}
    \begin{center}
        \ifdefvoid{\theauthor}{}{\textit{Author:} \theauthor}
    \end{center}
    
    \vspace{\fill}
    \begin{center}
        \transparentlogo[width=108pt]

        \href{https://transparent.us}{transparent.us}
    \end{center}
    \clearpage
}

\define@key{xandtitle}{abstracttitle}{\renewcommand{\abstractname}{#1}}
\define@key{xandtitle}{logospace}{\setlength{\logospace}{#1}}
\define@key{xandtitle}{keywords}{\hypersetup{pdfkeywords={#1}}}
\newcommand{\xandtitle}[1][]{
    \setkeys{xandtitle}{#1}
    
    \hypersetup{
        pdftitle=\thetitle,
        pdfauthor=\ifdefvoid{\theauthor}{\thecompany}{\theauthor},
    }
    \setlength{\droptitle}{\logospace}
    \renewcommand{\maketitlehooka}{\centering\xandlogo[width=144pt]\par\vspace*{\logospace}}
    \renewcommand{\maketitlehookc}{\centering\vspace{4ex}}

    \maketitle
    \ifdefvoid{\theabstract}{}{
        \begin{abstract}
            \noindent\parindent=0pt\parskip=1\baselineskip plus 2pt
            \theabstract
        \end{abstract}
    }
    \hypersetup{pageanchor=true}
}

\define@boolkey{xandcontents}{nonewpageforlists}[true]{}
\define@boolkey{xandcontents}{figures}[true]{}
\define@boolkey{xandcontents}{tables}[true]{}
\define@key{xandcontents}{tocdepth}{\setcounter{tocdepth}{#1}}
\newcommand{\xandcontents}[1][]{
    \setkeys{xandcontents}{#1}

    \pagenumbering{roman}
	\tableofcontents
    \ifKV@xandcontents@nonewpageforlists
        \vspace{3ex plus 2ex}
    \else
        \clearpage
        \thispagestyle{plain}
    \fi
    
    \ifKV@xandcontents@figures \listoffigures \else\fi
    \ifKV@xandcontents@tables \listoftables \else\fi
    \clearpage
    \pagenumbering{arabic}
}

%--------------------------------------
% Changelog commands
%--------------------------------------
\gdef\lastchangedate{}
\gdef\@thechangelog{}
\newenvironment{changelog}{
    \newcommand{\addchange}[2]{
        \gdef\lastchangedate{##1}
        \def\myitem{\item[##1] ##2}
        \xdef\@thechangelog{\unexpanded\expandafter{\@thechangelog} \unexpanded\expandafter{\myitem}}
    }
}{\undef{\addchange}}

\newcommand{\makechangelog}{
    \ifdefempty{\@thechangelog}{}{
        \hypertarget{sec:changelog}{\section*{Changelog}}
        \begin{description}
            \@thechangelog
        \end{description}

        \gdef\@thechangelog{}
    }
}

\AtEndDocument{\makechangelog}
