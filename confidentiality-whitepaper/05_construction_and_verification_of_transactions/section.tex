\section{Construction and Verification of Transactions}
\subsection{Overview}

The following is an informal description of the pieces that go into the proof and what their purpose is. Not all transactions contain all components. The Xand network requires proof of a set of facts about a transaction before it can be added to the ledger. Primarily we must prove the following facts about the transaction:

\begin{itemize}
	\item The submitter of the transactions owns the real inputs they are using
	\item Those real inputs have not previously spent in another transaction
	\item The total of the real inputs is equal to the outputs
	\item The submitter is a Xand Member
\end{itemize}

In order to do this we construct a linear tuple with the following form
$$\mathbf{Y}_k =(\forall_l(A_{kl},B_{kl}), \forall_l(H_{kl}, I_l), (Z,\sum_l V_{kl} - \sum_t V_t), (S_k, T_k), (S,T))$$

Each element in this tuple corresponds to one of the four facts about the transaction above and will be explained below. We then provide privacy by hiding this constructed linear tuple among other generated non-linear tuples and providing a proof that one of the tuples is linear called a ring signature. This proof of linearity proves the four facts above.


\paragraph{Transaction Inputs:} We start with list of $m$ sets of input commitments, each of length $n$:  $\forall_j \forall_l(A_{jl},B_{jl},V_{jl})$ where $j \in \{1,\ldots,m\}$, $l \in \{1,\ldots,n\}$, and one set of output commitments $\forall_t(A_t, B_t, V_t)$ where $t \in \{1,\ldots,o\}$. $o$, $j$ and $l$ all represent indices. $k$ is particular value of $j$ used to identify the index of the true inputs. We also consider a list of the permanent public keys of the banned Members $\forall_eP_e$, where $e \in \{1...u\}$.

$$\mathsf{Inputs} = \begin{bmatrix}
		\iota_{1,1} & \ldots & \iota_{1,n} \\
		\vdots      & \ddots &             \\
		\iota_{m,1} &        & \iota_{m,n}
	\end{bmatrix}\quad \text{where}\quad \iota_{j,l} = (A_{jl}, B_{jl}, V_{jl})$$

Choose $m-1$ instances $\forall_{j \in \{1,2,..,m\}\setminus \{k\}}\forall_l(A_{jl},B_{jl},V_{jl})$ from other available TxOs in the blockchain these are the input TxOs that will be used to mask which inputs the transaction constructor owns.

\paragraph{Transaction Outputs:} These transactions have outputs of the form: $$\mathsf{Outputs} = \{ \psi_{1}, \ldots, \psi_{o} \} \quad \text{where}\quad \psi_{t} = (A_t, B_t, V_t)$$, where $t$ is the index of the output.

\paragraph{Proof that the submitter owns the real inputs $\forall_l(A_{kl},B_{kl})$:} The submitter's private key is a scalar $p$. We can have multiple public keys for the same private key. The pair of curve points $(A,B)$ is a public key for the private key $p$ if and only if $B=pA$. So, if we have a list of public keys $\forall_{l \in \{1...n\}}(A_{kl},B_{kl})$, then for every $l \in \{1...n\}$, $B_{kl}=pA_{kl}$. So, the tuple $(\forall_{l \in \{1...n\}}(A_{kl},B_{kl}))$ is a linear tuple by our definition.

\paragraph{Proof that the real inputs have not been spent before $\forall_l(H_{kl}, I_l)$:} Since the validators cannot know which TxOs are being spent, we need some nullifier or key-image to mark them down after they are spent. For every TxO $(A_{kl},B_{kl},V_{kl})$ that is being used, the hash $H_{kl}$ is computed as  $H_{kl}=H_g(A_{kl},B_{kl})$. This is a unique hash identifying the TxO being used. The key-image ${I_l}$ is then computed as $I_l=pH_{kl}$. As the hashes are fixed for a given TxO, so is the key-image. Since the key-images cannot be used more than once, the input to the hash, in this case $(A_{kl},B_{kl})$, can be used as the real input only once. We could have used $(A_{kl},B_{kl}, V_{kl})$ instead as the input to the hash, which would allow use of the same public key in another TxO with a different value. This is not necessary as temporary public keys are only meant to be used once. We also need a proof that the key-image is computed correctly. Notice that the both slope of the vector $(A_{kl},B_{kl})$ and $(H_{kl},I_l)$ are the same value $p$. So, the tuple $(\forall_l(A_{kl},B_{kl}), \forall_l(H_{kl}, I_l))$ is a linear tuple.

\paragraph{Proof that the total of the real inputs is equal to the outputs $(Z,\sum_l V_{kl} - \sum_t V_t)$:} We must also ensure the sum of the input commitments being used is the same as the sum of the output commitments to verify no value was created or destroyed. However, we cannot do it directly since we don't want to disclose which tuple of the inputs we are actually using. Instead, we prove that the difference between the sum of the output commitments minus the sum of the input commitments is a commitment to zero. The commitment to a value $v$ is $rG+vL$ for any $r$, therefore the commitment to zero is $rG$ for some scalar $r$. For the input commitments $V_{kl}$ and output commitments $V_{t}$, we make sure that $\sum_l V_{kl} -  \sum_t V_t = rG$ for some scalar $r$. We also want to make it part of the linear tuple we are building, so a natural choice for the value of $r$ is $p$. However, if we take that, $rG$ would always be $pG=P$, the permanent public key of the signer. So, we must add additional randomness. We first choose a random scalar $z$, and then choose $r=pz$. We then compute $Z=zG$ and compute $\mathbf{Z} = pZ$. This means $\mathbf{Z} = \sum_l V_{kl} - \sum_t V_t$. Now, the slope of $(Z,\mathbf{Z}) = (Z,\sum_l V_{kl} - \sum_t V_t)$ is also $p$. Hence, the tuple $(\forall_l(A_{kl},B_{kl}), \forall_l(H_{kl}, I_l), (Z,\sum_l V_{kl} - \sum_t V_t))$ is a linear tuple.

\paragraph{Proof that the submitter is a Xand Member $(S_k, T_k), (S,T)$:} We must also prove that the signer is an authorized Member. We use Membership keys of the form $(S,T)$ for this. The Membership keys are structurally the same as the temporary public keys, i.e. if $(S,T)$ is a Membership key for the private key $p$, then $T=pS$. The Membership keys for a particular Member forms a chain. Every transaction has a true input Membership key which is $(S_k,T_k)$ and an output Membership key $(S,T)$. Since they are the Membership keys for the same Member, the slope of both of them is $p$. Hence, the tuple $\mathbf{Y}_k =(\forall_l(A_{kl},B_{kl}), \forall_l(H_{kl}, I_l), (Z,\sum_l V_{kl} - \sum_t V_t), (S_k, T_k), (S,T))$ is a linear tuple.

\paragraph{Protecting the Submitter's privacy:} To obfuscate our source transaction, we make other tuples with the same structure, which are not necessarily linear. The values $\forall_l I_l, (S,T),\forall_t (A_t,B_t,V_t)$ are the outputs of the transaction, i.e. these values are created in the transaction. We keep these the same in all of the fake inputs. Others we fill in with different values from the blockchain. For example, the fake input TxO $(A_{jl},B_{jl},V_{jl})$ are picked from existing TxOs in the blockchain. $\mathbf{Y}_j =(\forall_l(A_{jl},B_{jl}), \forall_l(H_{jl}, I_l), (Z,\sum_l V_{jl} - \sum_t V_t), (S_j, T_j), (S,T))$. We hide the linear tuple within the non-linear tuples with the same structure that we have created.

\paragraph{Constructing the Proof:} Finally, we simply have to prove that in the list of tuples $\forall_j \mathbf{Y}_j$, there is some $k$, for which $\mathbf{Y}_k$ is a linear tuple, and all of the above properties will apply to the tuple at that index $k$. This proof of the existence of the linear tuple would be called $\pi$.

However, we have to add one more thing. Notice that $\pi$ only proves $\sum_l V_{kl} - \sum_t V_t = pZ$. $pZ$ is a commitment to zero only when there is some $z$ known to the prover such that $Z=zG$. The knowledge of the value of $z$ is proved with a second ZkPLMT called $\alpha$.

\paragraph{Banned List:} We need to prevent previous Members who have been banned from participating on the Network, therefore need to check that a particular signer is not on the banned list. $\alpha$ also proves that for every $e$, $Q_e=zP_e$. Since $\pi$ already proves that $\sum_l V_{kl} - \sum_t V_t = pZ$, we only need to check that for every $e$ and every $j$, $\sum_l V_{jl} - \sum_t V_t \neq Q_e$ to make sure that for every $e$, $Q_e \neq pZ$. Now, since $Q_e = zP_e$, this check also proves that $P_e \neq pG$. $pG$ is the permanent public key of the signer, so this proves that the signer is not in the banned list.

\paragraph{Disclosure of the sender's identity:} Let an output TxO be $(V_t,A_t,B_t)$. Let the public key of the signer be $P$. The public key is simply encrypted using the El-Gamal encryption with the public key $(A_t,B_t)$. To do that, first a random $x$ is chosen from $\mathbb{F}_q$, and then the encryption is computed as $E=xB_t+P$. Both $X=xA_t$ and $E$ are stored in the transaction. The receiver knows the private key of the receiver (say $q_t$); so, the receiver can compute the value of $P$ as $E-qX$. Notice that $B_t=qA_t$, so $qX=qxA_t = xB_t$. This means $E-qX = xB_t+P-xB_t=P$.

Now, the trick is to prove that $E$ indeed is equal to $xB_t+P$ to the validators who don't know $q$ and must not be able to decrypt $E$. The trick is to 'promote' everything by some secret random $y$ and then check the sum. We compute $Y=yG,E'=yE, P'=yP$. We now want to show that $E'=xyB_t+P'$, which would then prove that $E=xB_t+P$. To do this, we must also compute the value of $xyB_t$. We first compute $B_t'=yB_t$ and then $Q'=xB_t' = xyB_t$. To prove the promotion is correct, we compute a ZkPLMT ($\pi_1$) for the linear tuple $(G,Y),(E,E'),(B'_t,yB_t)$. We also compute a ZkPLMT ($\pi_2$) to prove the linearity of the tuple $(A_t,X),(B_t',Q')$. And finally, we add the pair $(Y,P')$ to the main ZkPLMT to prove that $P'=pY=yP$. $\pi_1$ proves that $B'_t=yB_t$ and $E'=yE$. $\pi_2$ proves $Q'=xB_t' = xyB_t$. The main signature proves that $P'=yP$. Now, the verifier can simply check whether $E'=Q'+P'$ to know whether the encryption is correctly done.

\paragraph{Unsigned Transaction:} We assume that all relevant unsigned transaction data is $M \in \{0,255\}^*$.
\subsection{Input Selection}
We propose that transaction constructors use the following methods for selecting real and decoy inputs.

\subsubsection{Input TxO Selection}
For minimizing dust, select TxOs to make up nearly double the amount that is actually being paid so as to make the real payment and the change amount equal. This minimizes the dust creation as most UTxOs would end up having value almost the average transaction amount.

\subsubsection{Decoy Selection}

We propose an $N$-age-bound random selection -

\begin{itemize}
	\item We first fix a system-wide integer $N$ that is the bound in the transaction age
	\item Choose $k \leftarrow \{1...N\}$
	\item Select $k-1$ transactions created just before the true TxO in the order of creation in the blockchain. Let the set selected be $\mathcal{P}$
	\item Select $N-k$ transactions after the true TxO in the order of creation in the blockchain. If $N-k$ transactions are not available after the true TxO, repeat from the beginning. Let the set selected be $\mathcal{S}$. Let the true TxO be called $TTxO$
	\item Select $m-1$ TxOs from the set $(\mathcal{P} \cup \mathcal{S}) \setminus \{TTxO\}$ with a uniform distribution
\end{itemize}

\subsection{Activity Proofs}
\subsubsection{Confidential Transaction}

The signature of a transaction is similar to the CryptoNote signature, but we add a lot more features. Our signature is closer to the MLSAG signature used in Monero in terms of features, except we also include proof of Membership of the signer. Our signature is, in general, more space efficient than MLSAG.

The signature creator must choose input TxOs that they control, which we represent as: $(A_l,B_l,V_l)$ where $A_l$ and $B_l$ are the two curve points representing the $\mathsf{RecipientOneTimeKey}$ defined in our description of a TxO above. The subscript $l$ is an index into the list of all input TxOs.

\subsubsection{Create Transaction Proof}

\paragraph{Inputs}

\begin{itemize}
	\item True Membership tag $(S_1,T_1)$
	\item Private Key $p$
	\item Encryption private key $x$
	\item Output Values $\forall_t v_t$
	\item List of $m$ masking input Membership-tags $\forall_{j \in \{2...m\}} (S_j,T_j)$
	\item List of $u$ banned Member public keys $\forall_e P_e$
	\item Account number $a$
	\item Routing number $b$
	\item Decryption key of the trust $D$
	\item Banking Correlation ID $\mathsf{corrId}$ (16 bytes)
	\item Receiver or trustee's public key $(A,B)$, where $B = qA$, $q$ is the private key of the receiver or trustee
\end{itemize}

\paragraph{Construction}

\begin{itemize}
	\item Generate random $z \leftarrow \mathbb{F}_q^*$
	\item For all $e$, compute $Q_e =zP_e$
	\item Compute $Z = zG$

	\item Define $\mathbf{X}' = ((G,Z),\forall_{e}(P_e,Q_e))$
	\item Compute $\alpha = \mathbf{P}(\phi,\mathbf{X}', 1, z)$

	\item Compute $\mathbf{Z} = pZ$

	\item Shuffle Membership tags $\forall_j(S_j,T_j)$ randomly, so that $(S_k,T_k)$ is a Membership tag of the signer $(S,T)$ for some $k \in \{1...m\}$. This operation will reset the index $j$
	\item Construct output Membership tag $(S,T)$ such that $T=pS$
	\item For all $t$, construct $r_t, z_t  \leftarrow \mathbb{F}_q$
	\item For all $t$, construct output TxO $(A_t, B_t,V_t) = (z_tG, z_tpG, r_tG+v_tL)$

	\item Compute $\omega', F$ using~\autoref{shared-key-encryption}, where $\omega = (a, b)$ and $D$ is the Trust encryption key
	\item Sample $x,y \gets \mathbb{F}_q^*$
	\item Compute $X = xG, Y=yG, E = xQ+P, P'=yP, E'=yE, B'=yQ, Q'=xyQ$. Note that $E=qX+P$, where $q$ is the private key of the trustee and $Q=qG$ is the public key of the trustee
	\item Compute proof $\pi_1 = \mathbf{P}(\emptyset, \{(G,Y), (E,E'), (Q,B')\}, 1, y)$
	\item Compute proof $\pi_2 = \mathbf{P}(\emptyset, \{(G,X), (B',Q')\}, 1, x)$
	\item For the output TxOs $\forall_t(A_t,B_t,V_t)$, construct linear tuple $\mathbf{Y}_k = (\forall_t(A_t,B_t), (Z, \mathbf{Z}), (S_k,T_k),(S,T), (Y,P'))$
	\item Construct nonlinear tuples $\mathbf{Y}_j = (\forall_t(A_t,B_t), (Z, \mathbf{Z}), (S_j,T_j),(S,T), (Y, P'))$ for all $j \neq k$
	\item Compute $\beta = \mathbf{P}(M,\forall_j\mathbf{Y}_j,k,p)$, where $M$ is the body of the transaction
	\item Compute $\bar{r} = \sum_{t=1}^u r_t$ and $\bar{v} = \sum_{t=1}^u v_t$

	\item Output
	      \begin{align*}
		       & (\forall_t(A_t,B_t, V_t),\bar{r},\bar{v},(Z, \mathbf{Z}), \forall_j (S_j,T_j), \\
		       & (S,T), (Y, P'), Y, (E,E'), (B,B'), X, (Q,Q'), \forall_eQ_e,                    \\
		       & \alpha, \beta, \pi_1, \pi_2, \omega', F, \mathsf{corrId})
	      \end{align*}
\end{itemize}

\noindent \textbf{Verification}
\begin{itemize}
	\item Define $\mathbf{X}' = ((G,Z),\forall_{e}(P_e,Q_e))$

	\item Verify $\mathbf{V}(\phi,\mathbf{X}',\alpha)$. If the verification fails, return $false$

	\item $\forall_e$, check that $\mathbf{Z} \neq Q_e$. If the check fails, return $false$

	\item Verify that the values $\forall_j(S_j,T_j)$ are present as Membership-tag outputs of some transaction
	\item Construct nonlinear tuples $\mathbf{Y}_j = (\forall_t(A_t,B_t), (S_j,T_j),(S,T))$ for all $j$
	\item Verify $\mathbf{V}(M,\forall_j\mathbf{Y}_j,\beta)$. If the verification succeeds, return $true$ or return $false$

	\item Verify $\mathbf{V}(\emptyset, \{(G,Y), (E,E'), (Q,B')\}, \pi_1)$. If the verification succeeds, return true or return false

	\item Verify $\mathbf{V}(\emptyset, \{(G,X), (B',Q')\}, \pi_2)$. If the verification succeeds, return true or return false
	\item Verify that the $\mathsf{corrId}$ has never been used before for any other create or redeem request
	\item Verify that $E' = Q'+P'$. If the verification fails, return false
	\item Verifier can reveal the sender's identity by computing $P = E-qX$
\end{itemize}

\paragraph{Explanation}
\begin{itemize}

	\item $\alpha$ proves that if $Z=zG$, then for each $e$, $Q_e=zP_e$.

	\item $\beta$ only proves that for some $k$, $(\forall_t(A_t,B_t),(Z,\mathbf{Z}), (S_k,T_k),(S,T))$ is a linear tuple. This means that for all $t$, $(A_t,B_t)$ has the same slope as $(S_k,T_k)$. This means that $(A_t,B_t)$ belongs to a Member. It also means that $(S,T)$ has the same slope and hence can work as a Membership tag in future transactions. It also means, that $\mathbf{Z} = pZ$ which is then checked to not match with any $Q_e$.

	\item $\pi_2$ proves that $Q' = xB'$ given $X = xG$.

	\item $\pi_1$ proves that $E' = yE$ and $B' = yQ$ given $Y=yG$. Combining $\pi_1$ and $\pi_2$, we prove that $E' = xB'+P' = Q'+P'$.

\end{itemize}

Also, in the main proof $\beta$ we prove that $T = pS$ given $yP = pY$. Thus combining $\pi_1, \pi_2, \beta$, we have proved that $E$ is an encryption of $P$ such that $P=pG$ and $T = pS$. Thus, the verifier can reveal the real identity of sender by computing $P = E-qX$ using his own private key $q$.



\subsubsubsection{Construction of Input for Send UTxOTX}

\begin{itemize}
	\item Choose $z \leftarrow \mathbb{F}_q^*$ (As a randomness to hide the link between the input commitments and the output commitments)
	\item Generate random curve point $S$ (As a base point for the output KYC)
	\item Save index $k \in \{1..m\}$ for the linear tuple. $k$ is the index of the tuple that the transaction constructor owns / has the right to spend
	\item Compute $Z = zG$
	\item Let the list of banned Members be $(P_1,P_2,...,P_u)$
	\item $\forall e \in \{1...u\}$, compute $Q_e=zP_e$
	\item Define $\mathbf{X}' = ((G,Z),\forall_{e}(P_e,Q_e))$
	\item Compute $\alpha = \mathbf{P}(\phi,\mathbf{X}', 1, z)$
	\item Choose output commitments $\forall_t V_t$ such that  $\sum_l V_{kl} = \mathbf{Z} + \sum_t V_t$,  where $\mathbf{Z} = pZ$
	\item $\forall j, \forall l$, compute $H_{jl} = H_g (A_{jl},B_{jl})$
	\item $\forall l$, compute key-image $I_l = pH_{lk}$ and declare $I_l$
	\item Compute $T = pS$. The KYC output is $(S,T)$
	\item Sample $x,y \gets \mathbb{F}_q^*$
	\item For each $t$, compute $X_t = xA_t, Y = yG, E_t=xB_t+P, P'=yP, E'_t=yE_t, B'_t=yB_t, Q'_t=xyB_t$. Note that $E_t=q_tX+P$, where $q_t$ is the private key of the receiver, i.e. $B_t=q_tA_t$
	\item Choose $(m-1)$ KYC outputs from all the past transactions $\forall_{j \in \{1...m\} \setminus \{k\}}(S_j, T_j)$
	\item Create the linear tuple $\\\mathbf{Y}_k = (\forall_l(A_{kl},B_{kl}),\forall_l(H_{kl}, I_l) , (Z,\mathbf{Z}), (S_k,T_k), (S,T), (Y,P'))$
	\item $\forall j \neq k$, create the non-linear tuples
	      \begin{align}
		      \begin{split}
			      \mathbf{Y}_j = (&\forall_l(A_{jl},B_{jl}), \\
			      &\forall_l(H_{jl}, I_l), \\
			      &(Z, \sum_l V_{jl} -\sum_t V_t), \\
			      &(S_j, T_j), (S,T), (Y,P'))
		      \end{split} \nonumber
	      \end{align}
	\item For each $t$, compute $z_t = H_q(M',p,t)$, where $M'$ is the body of the transaction without the output TxOs and the proof
	\item For each $t$, Compute $(A_t,B_t) = (z_tG, z_tP_t)$ where $P_t$ is the permanent public of the $t^{th}$ receiver
	\item For each $t$, compute $\omega_t'$ using~\autoref{shared-key-encryption}, where $\omega'_t = (z_t, v_t)$ and $D$ is the encryption key owned by $P_t$

	\item Create proof $\pi = \mathbf{P}(M,\forall_j\mathbf{Y}_j,k, p)$ where $M$ is the body of the transaction without the proof
	\item Generate proof $\pi_1 = \mathbf{P}(\emptyset, \{(G,Y), \forall_t(E_t,E'_t), \forall_t(B_t,B'_t)\}, 1, y)$
	\item Generate proof $\pi_2 = \mathbf{P}(\emptyset, \forall_t\{(A_t,X_t), \forall_t(B'_t,Q'_t)\}, 1,x)$
	\item Output
	      \begin{align*}
		       & (\forall_j\forall_l(A_{jl},B_{jl},V_{jl}), \forall_j (S_j, T_j), (S,T),                             \\
		       & \forall_t(A_t,B_t,V_t,\omega_t'),Z,(Y,P'), (G,Y), \forall_t(E_t,E'_t), \forall_t(B_t,B'_t),         \\
		       & \forall_t(A_t,X_t), \forall_t(B'_t,Q'_t), \pi_1, \pi_2, \forall_lI_l, \forall_eQ_e, \alpha, \pi, F)
	      \end{align*}
\end{itemize}

\paragraph{Verification}
\begin{itemize}
	\item $\forall j$ and $\forall l$, check that $(A_{jl},B_{jl},V_{jl})$ exists in the blockchain as outputs of the past transactions. If not, return $false$
	\item $\forall j$, check that $(S_j,T_j)$ is present in the past transactions in the blockchain. If not, return $false$
	\item $\forall l$, check that $I_l$ is not present in the list of key-images for any past transaction. If present, return $false$
	\item Define $\mathbf{X}' = ((G,Z),\forall_e(P_e,Q_e))$
	\item Verify $\mathbf{V}(\phi,\mathbf{X}',\alpha)$. If the verification fails, return $false$
	\item $\forall j, \forall l$, compute $H_{jl} = H_g (A_{jl},B_{jl})$
	\item $\forall j$, create the tuples
	      \begin{align}
		      \begin{split}
			      \mathbf{Y}_j = (&\forall_l(A_{jl},B_{jl}), \\
			      &\forall_l(H_{jl}, I_l), \\
			      &(Z, \sum_l V_{jl} - \sum_t V_t), \\
			      &(S_j, T_j), (S,T), (Y,P'))
		      \end{split} \nonumber
	      \end{align}
	\item Verify that $\forall j$ and $\forall e$, $Q_e \neq \sum_l V_{jl} - \sum_t V_t$. If any of the inequality is violated, return false
	\item Verify $\mathbf{V}(M,\forall_j\mathbf{Y}_j,\pi)$. If the verification succeeds, return $true$, else return false
	\item Verify $\mathbf{V}(\emptyset, \{(G,Y), \forall_t(E_t,E'_t), \forall_t(B_t,B'_t)\}, \pi_1)$. If the verification fails, return false. Otherwise, return true
	\item Verify $\mathbf{V}(\emptyset, \{\forall_t(A_t,X_t), \forall_t(B'_t,Q'_t)\}, \pi_2)$. If the verification fails, return false. Otherwise, return true
	\item $\forall t$, verify that $E'_t=Q'_t+P'_t$. If the verification fails, return false. Otherwise, return true
	\item Verifier can reveal the sender's identity by computing $P=E-q_tX$
\end{itemize}

\subsubsubsection{Construction of Input for RedeemUTxOTX}

\begin{itemize}
	\item Set the Banking Correlation ID: $\mathsf{corrId}$ (16 bytes)
	\item Choose $z \leftarrow \mathbb{F}_q^*$ (As a randomness to hide the link between the input commitments and the output commitments)
	\item Generate random curve point $S$ (As a base point for the output KYC)
	\item Save index $k \in \{1..m\}$ for the linear tuple. $k$ is the index of the tuple that the transaction constructor owns / has the right to spend
	\item Compute $Z = zG$
	\item Let the list of banned Members be $(P_1,P_2,...,P_u)$
	\item $\forall e \in \{1...u\}$, compute $Q_e=zP_e$
	\item Define $\mathbf{X}' = ((G,Z),\forall_{e}(P_e,Q_e))$
	\item Compute $\alpha = \mathbf{P}(\phi,\mathbf{X}', 1, z)$
	\item Choose output commitments $V_c$ for change and $V_r$ for redemption amount, such that  $\sum_l V_{kl} = \mathbf{Z} + V_c + V_r$,  where $\mathbf{Z} = pZ$
	\item $\forall j, \forall l$, compute $H_{jl} = H_g (A_{jl},B_{jl})$
	\item $\forall l$, compute key-image $I_l = pH_{lk}$ and declare $I_l$
	\item Compute $T = pS$. The KYC output is $(S,T)$
	\item Sample $x,y \gets \mathbb{F}_q^*$
	\item Choose $(m-1)$ KYC outputs from all the past transactions $\forall_{j \in \{1...m\} \setminus \{k\}}(S_j, T_j)$,
	\item Create the linear tuple $\\\mathbf{Y}_k = (\forall_l(A_{kl},B_{kl}),\forall_l(H_{kl}, I_l) , (Z,\mathbf{Z}), (S_k,T_k), (S,T), (Y,P'))$
	\item For each $(redemption (r), change (c))$, choose random r: $r_r,r_c \leftarrow \mathbb{F}_q^*$ (As a randomness to differentiate output public keys)
	\item Compute $(A_r, B_r) = (r_rG, r_rP')$ where $P'$ is the permanent public key of the redeemer
	\item Compute $(A_c, B_c) = (r_cG, r_cP')$ where $P'$ is the permanent public key of the redeemer
	\item $\forall j \neq k$, create the non-linear tuples
	      \begin{align}
		      \begin{split}
			      \mathbf{Y}_j = (&\forall_l(A_{jl},B_{jl}), \\
			      &\forall_l(H_{jl}, I_l), \\
			      &(Z, \sum_l V_{jl} -V_r -V_c), \\
			      &(S_j, T_j), (S,T), (Y,P'), (A_r, B_r), (A_c, B_c)) \\
		      \end{split} \nonumber
	      \end{align}
	\item Compute $\omega', F$ using~\autoref{shared-key-encryption}, where $\omega = (a, b)$ and $D$ is the Trust encryption key

	\item Create proof $\pi = \mathbf{P}(M,\forall_j\mathbf{Y}_j,k, p)$ where $M$ is the body of the transaction without the proof
	\item Compute $X = xG, Y=yG, E = xQ+P, P'=yP, E'=yE, B'=yQ, Q'=xyQ$. Note that $E=qX+P$, where $q$ is the private key of the trustee and $Q=qG$ is the public key of the trustee
	\item Compute proof $\pi_1 = \mathbf{P}(\emptyset, \{(G,Y), (E,E'), (Q,B')\}, 1, y)$
	\item Compute proof $\pi_2 = \mathbf{P}(\emptyset, \{(G,X), (B',Q')\}, 1, x)$
	\item Output
	      \begin{align*}
		       & (\forall_j\forall_l(A_{jl},B_{jl},V_{jl}), \forall_j (S_j, T_j), (A_c,B_c,V_c), (A_r,B_r,V_r), \\
		       & (S,T), (Y,P'),Z, Y, (E,E'), (Q,B'), X, (B',Q'), \pi_1, \pi_2, \forall_lI_l,                    \\
		       & \forall_eQ_e, \alpha, \pi, \pi_1, \pi_2, \omega', F, \mathsf{corrId})
	      \end{align*}
\end{itemize}

\paragraph{Verification}
\begin{itemize}
	\item $\forall j$ and $\forall l$, check that $(A_{jl},B_{jl},V_{jl})$ exists in the blockchain as outputs of the past transactions. If not, return $false$
	\item $\forall j$, check that $(S_j,T_j)$ is present in the past transactions in the blockchain. If not, return $false$
	\item $\forall l$, check that $I_l$ is not present in the list of key-images for any past transaction. If present, return $false$
	\item Define $\mathbf{X}' = ((G,Z),\forall_e(P_e,Q_e))$
	\item Verify $\mathbf{V}(\phi,\mathbf{X}',\alpha)$. If the verification fails, return $false$
	\item $\forall j, \forall l$, compute $H_{jl} = H_g (A_{jl},B_{jl})$
	\item $\forall j$, create the tuples
	      \begin{align}
		      \begin{split}
			      \mathbf{Y}_j = (&\forall_l(A_{jl},B_{jl}), \\
			      &\forall_l(H_{jl}, I_l), \\
			      &(Z, \sum_l V_{jl} -V_r -V_c), \\
			      &(S_j, T_j), (S,T), (Y,P'), (A_r, B_r), (A_c, B_c)) \\
		      \end{split} \nonumber
	      \end{align}
	\item Verify that $\forall j$ and $\forall e$, $Q_e \neq \sum_l V_{jl} - V_c - V_r$. If any of the inequality is violated, return false
	\item Verify $\mathbf{V}(M,\forall_j\mathbf{Y}_j,\pi)$. If the verification succeeds, return $true$, else return false
	\item Verify that the $\mathsf{corrId}$ has never been used before for any other Create or Redeem request
	\item Verify $\mathbf{V}(\emptyset, \{(G,Y), (E,E'), (Q,B')\}, \pi_1)$. If the verification fails, return false
	\item Verify $\mathbf{V}(\emptyset, \{(G,X), (B',Q')\}, \pi_2)$. If the verification fails, return false
	\item Verify that $E' = Q'+P'$. If the verification fails, return false
	\item Verifier can reveal the sender's identity by computing $P=E-qX$
\end{itemize}

\paragraph{Explanation}
$\alpha$ simply works as a proof of knowledge of the discrete log $z$ of $Z$ with respect to $G$. The proof $\pi$ proves that one of the tuples in $\forall_j\mathbf{Y}_j$ is linear. For such a tuple $\mathbf{Y}_k = (\forall_l(A_{kl},B_{kl}),\forall_l(H_{kl}, I_l) ,\\ (Z,\mathbf{Z}), (S_k,T_k), (S,T), (Y,P'))$, the following can be concluded

\begin{itemize}
	\item All public keys $\forall_l(A_{kl},B_{kl})$ correspond to the same private key since the tuple is linear due to the soundness theorem.
	\item The signer knows the private key for the public keys $\forall_l(A_{kl},B_{kl})$ by the proof of knowledge theorem.
	\item Since $\forall_l(A_{kl},B_{kl}),\forall_l(H_{kl}, I_l)$ are linear; we know that the key-images $\forall_lI_l$ are correctly computed. Since for every $l$, $I_l = pH_{kl}$ and $H_{kl}$ values are fixed and publicly known for $(A_{kl},B_{kl})$, this makes sure that if any of the source UTXOs is ever used again, the signer has to compute the same value for the corresponding key-image. This prevents double-spending the same way as CryptoNote.
	\item $(Z,\mathbf{Z})$ is linear with the public keys, meaning $\mathbf{Z}=pZ=pzG$, which makes the sum of the transaction amounts in the input is equal to the sum of the transaction amounts in the output since the discrete log of the difference in the commitments with respect to $G$ is clearly known to the signer.
	\item $\alpha$ also proves that $\forall e, Q_e=zP_e$. Since $\forall j$ and $\forall e$, $Q_e \neq \sum_l V_{jl} - \sum_t V_t = pZ = zpG \Rightarrow pG \neq P_e$. This proves that the signer's public key is not in the banned list.
	\item $\pi_2$ proves that $Q' = xB'$ given $X = xG$.
	\item $\pi_1$ proves that $E' = yE$ and $B' = yQ$ given $Y=yG$. Combining $\pi_1$ and $\pi_2$, we prove that $E' = xB'+P' = Q'+P'$.
\end{itemize}
Also, since in the main proof $\beta$ we prove that $T = pS$ given $yP = pY$. Thus combining $\pi_1, \pi_2, \beta$, we have proved that $E$ is an encryption of $P$ such that $P=pG$ and $T = pS$. Thus, the verifier can reveal the real identity of sender by computing $P = E-qX$ using his own private key $q$.

\subsection{Proving Information to Third Parties}
For various reasons a Member of the network might want to be able to prove the amount of Claims that they own on the network, as well as the transactions that they were involved or not involved in. The below proofs allow them to prove facts to any third party that can also see the ledger of transactions.
\subsubsection{Proof of ownership and non-ownership of a TxO} Given a TxO $(A,B,V)$, the following procedure proves the ownership or non-ownership for a key-pair $(p,P)$.

\paragraph{Proof}
\begin{itemize}
	\item Compute $B' = pA$.
	\item Construct the linear tuple $\mathbf{X}=((G,P),(A,B'))$.
	\item Compute ZkPLMT $\pi = \mathbf{P}(\phi, (\mathbf{X}),1,p)$.
	\item Return $(\pi,B')$.
\end{itemize}
\paragraph{Verification}
\begin{itemize}
	\item Construct tuple $\mathbf{X}=((G,P),(A,B'))$.
	\item Verify $\mathbf{V}(\phi, (\mathbf{X}),\pi)$.
	\item If verification fails, reject the proof.
	\item If verification succeeds, $B'=B$ implies the TxO is owned by the owner of the public key $P$, and $B \neq B'$ implies the TxO is not owned by the owner of the public key $P$.
\end{itemize}

\subsubsection{Proof of unspent-ness} The owner of a TxO can prove that the TxO has not been spent if that is the case.
Given a TxO $(A,B,V)$ and a key-pair $(p,P)$, do the following --
\paragraph{Proof}
\begin{itemize}
	\item Compute $H = H_g(A,B)$.
	\item Compute $I = pH$.
	\item Compute the linear tuple $X=((A,B),(H,I))$.
	\item Compute $\pi = \mathbf{P}(\phi,X,1,p)$.
	\item Output $(I,\pi)$.
\end{itemize}
\paragraph{Verification}
\begin{itemize}
	\item Compute $H = H_g(A,B)$.
	\item Compute the linear tuple $X=((A,B),(H,I))$.
	\item Compute verification $\mathbf{V}(\phi,X,\pi)$.
	\item If the verification does not succeed, reject the proof.
	\item Check if $I$ is already in the list of spend key-images. If so, reject the proof; otherwise accept the proof.
\end{itemize}

\subsubsection{Proof of creation or non-creation of transaction:}
For this purpose, given a transaction, we will only consider the output identity tag $(S,T)$. The owner of the key-pair $(p,P)$ can prove or disprove the creation of the transactions by themselves without disclosing which TxOs were actually spent in the following manner:
\paragraph{Proof}
\begin{itemize}
	\item Compute $T' = pS$.
	\item Construct the linear tuple $\mathbf{X}=((G,P),(S,T'))$.
	\item Compute ZkPLMT $\pi = \mathbf{P}(\phi, (\mathbf{X}),1,p)$.
	\item Return $(\pi,T')$.
\end{itemize}
\paragraph{Verification}
\begin{itemize}
	\item Construct tuple $\mathbf{X}=((G,P),(S,T'))$.
	\item Verify $\mathbf{V}(\phi, (\mathbf{X}),\pi)$.
	\item If verification fails, reject the proof.
	\item If verification succeeds, $T'=T$ implies the transaction was created by the owner of the public key $P$, and $T \neq T'$ implies the transaction was not created by the owner of the public key $P$.
\end{itemize}

\subsubsection{Proof of real source of the transaction:} In case the owner of the key-pair $(p,P)$ has originated a transaction, they can prove this fact along with the true source TxOs in the following manner. In this case, only the source identity tag $(S_k,T_k)$ corresponding to the true TxOs at index $k$ is chosen. $(S,T)$ is the output identity tag.

\paragraph{Proof}
\begin{itemize}
	\item Construct the linear tuple $\mathbf{X}=((G,P),(S_k,T_k),(S,T))$.
	\item Compute ZkPLMT $\pi = \mathbf{P}(\phi, (\mathbf{X}),1,p)$.
	\item Return $(\pi,k)$.
\end{itemize}
\paragraph{Verification}
\begin{itemize}
	\item Construct tuple $\mathbf{X}=((G,P),(S_k,T_k),(S,T))$.
	\item Verify $\mathbf{V}(\phi, (\mathbf{X}),\pi)$.
	\item If verification fails, reject the proof.
	\item If verification succeeds, it proves that the owner of the key-pair $(p,P)$ generated the transaction and also $k$ is the true index of the source TxOs.
\end{itemize}