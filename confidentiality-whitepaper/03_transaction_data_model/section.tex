\section{Data Model}
\label{sec:data-model}

%\begin{verse}
This section presents an overview of the data model for all transactions. It describes what data is within the payload of each transaction, the purpose of that payload, as well as how transactions interact with the state of the system.
%\end{verse}

\subsection{Components}
\subsubsection{Permanent Public Key}
Every Member of the Network has a single Private Key that they use for signing. This is a scalar value henceforth referred to as $p$. This private key has a corresponding curve point $P$ where $P=pG$ and is called the Permanent Public Key. $P$ represents the public identity of the Member with private key $p$ and is known to all parties on the network.

\subsubsection{One-Time Addresses}
	In order to obscure the owner of Claims, all Claims are given a One-Time Address. They are named such because each one is unique. A One-Time Address is two points $(A,B)$ such that $pA=B$ where $p$ is the owner's private key. Anyone can generate a new One-Time Address for anyone else using the below method:
	
	\begin{itemize}
		\item Select recipient $P$
		\item Choose $r \leftarrow \mathbb{F}_q$.
		\item Compute the temporary public key $(A,B) = (rG,rP)$.
	\end{itemize}

Any Member can know whether a TxO $(A,B,V)$ belongs to them with the help of the private key $p$. This is done using the check $B \stackrel{?}{=} pA$.

\subsubsection{Identity Tags}
Identity tags share an identical structure to One-Time Addresses. The only difference is their usage. Identity Tags are used as a tokenized version of a Member's authorization to transact on the network. Every transaction requires an Identity Tag from the pool of available tags and produces a new tag as the output to be added to the pool. Since there will be no Identity Tags available at the start of the network, $(G,P)$ is considered a valid Identity Tag for any Member with the Permanent Public Key $P$.

\subsubsection{Claims} 
Confidential Claims on the network take the form of $(A, B, V)$, where $A$ and $B$ are a One-Time Addresses for the recipient as described above and $V$ is a point representing a value commitment for the dollar value of the Claim. The value commitment can be computed as below:

\bigskip
$\mathsf{TxO}:$ Given a value $v$

\begin{itemize}
	\item Choose $r \leftarrow \mathbb{F}_q^*$
	\item Compute the value representation $V=rG+vL$.
\end{itemize}

Given a value commitment $V$, the only way to know the corresponding Claims value $v$ is to be given $v$ and $r$ and check $V\stackrel{?}{=}rG+vL$

\subsubsection{Opened Claims}
Opening a Claim allows any observer to know and verify the value of the Claim. Opening a Claim is as simple as providing $v$ and $r$. Any observer can verify that the $v$ provided is correct by checking $V\stackrel{?}{=}rG+vL$

\subsubsection{Key Images}
Key Images are used to prevent Claims from being double spent. Since there are multiple input sets to every transaction -- and the real input is obscure -- it is never publicly known which Claims have been spent. Each Key Image is linked to a one-time key. For any Claim $(A_{kl},B_{kl},V_{kl})$ t the hash $H_{kl}$ is computed as  $H_{kl}=H_g(A_{kl},B_{kl})$. The key-image ${I_l}$ is then computed as $I_l=pH_{kl}$. This formulation ensures that every Claim always produces the same Key Image and only the owner of the Claim who knows $p$ can produce the Key Image for a given Claim.

\subsection{Transactions}

\subsubsection{Create Request}
\label{sec:transaction:create_request}
\paragraph{Data:}
\begin{itemize}
	\item $identity\_inputs: \mathsf{Vec\langle IdentityTags\rangle}$, a set of identity tags. One of which is owned by the signer

	\item $outputs: \mathsf{Vec\langle OpenedClaims\rangle}$, a set of new Opened Claims
	
	\item $identity\_output$, a single newly created Identity Tag
	
	\item $correlation\_id: \mathsf{[u8; 16]}$, some randomly sampled value
	
	\item $encrypted\_sender: VerifiableEncryptionOfSignerKey$, this is an encryption of the signer key that can be proven to be correct by any observer
	
	\item $bank\_data: \mathsf{Vec\langle u8 \rangle}$, this contains the encrypted bank account and routing number
	
	\item $\beta:\mathsf{OutputProof}$, proof that the outputs belong to a verified Member identity
\end{itemize}
\paragraph{Preconditions:}
\begin{itemize}
	\item All the $identity\_inputs$ are valid known Identity Tags
	\item $correlation\_id$ has not been used before
	\item $outputs$ are not in the set of known Claims
\end{itemize}
\paragraph{Effects:}
\begin{itemize}
	\item The $identity\_output$ is added to the set of known Identity Tags
	\item A pending Creation is recorded with the $correlation\_id$
	\item $outputs$ are added to the set of known Claims as Pending
\end{itemize}

\subsubsection{Cash Confirmation}
\label{sec:transaction:cash_confirmation}
\paragraph{Data:}
\begin{itemize}
	\item $correlation\_id: \mathsf{[u8; 16]}$, The correlation ID of a previous Create Request
	\item $\beta:\mathsf{Signature}$, a regular Schnorr signature by the Trustee
\end{itemize}
\paragraph{Preconditions:}
\begin{itemize}
	\item $correlation\_id$ refers to a currently pending Creation
\end{itemize}
\paragraph{Effects:}
\begin{itemize}
	\item The pending creation with $correlation\_id$ is removed
	\item $outputs$ from the pending Creation are changed to be spendable
	\item The amount of all the $outputs$ is added to the total amount created
\end{itemize}

\subsubsection{Create Cancellation}
\label{sec:transaction:create_cancellation}
\paragraph{Data:}
\begin{itemize}
	\item $correlation\_id: \mathsf{[u8; 16]}$, the correlation ID of a previous create request
	\item $\beta:\mathsf{Signature}$, a regular Schnorr signature by the Trustee
\end{itemize}
\paragraph{Preconditions:}
\begin{itemize}
	\item $correlation\_id$ refers to a currently pending Creation
\end{itemize}
\paragraph{Effects:}
\begin{itemize}
	\item The pending creation with $correlation\_id$ is removed
	\item $outputs$ from the pending Creation are removed from the set of known Claims
\end{itemize}

\subsubsection{Send}
\label{sec:transaction:send}
\paragraph{Data:}
\begin{itemize}
	\item $input\_candidates: \mathsf{Vec\langle TransactionInputSet \rangle}$, a collection of TransactionInputSets, each of which contains a set of Claims. One of these sets is the real set being spent while the rest are decoys

	\item $identity\_inputs: \mathsf{Vec\langle Identity\_Tag\rangle}$, a set of identity tags. One of which is owned by the signer
	
	\item $outputs: \mathsf{Vec\langle Claims \rangle}$, a set of new Claims
	
	\item $identity\_output: \mathsf{IdentityTag}$, a single newly created Identity Tag
	
	\item $key\_images: \mathsf{Vec\langle KeyImage \rangle}$, a set of key images ensuring spent Claims cannot be spent again
	
	\item $Z: \mathsf{EdwardsPoint}$, used to store the randomness required to hide which of the input sum equals the output sum. Without this randomness, all anonymity will be lost.
	
	\item $\alpha: \mathsf{Proof}$, a proof that $Z$ is a Pedersen commitment to 0. It also implies that $pZ$ is also a commitment to 0 where $p$ is any scalar.
	
	\item $range\_proof: \mathsf{BulletRangeProof}$, proof that all the transaction output(s) contain commitments to values represented by a maximum number of bits. This is to stop the sum from rolling over to create money out of nothing
	
	\item $encrypted\_sender: VerifiableEncryptionOfSignerKey$, this is an encryption of the signer key that can be proven to be correct by any observer
	
	\item $extra: \mathsf{Vec\langle u8 \rangle}$, a collection of encrypted pieces of data, one for each Claim output. This contains the information needed for the recipient to open the Claim and see the amount
	
\end{itemize}
\paragraph{Preconditions:}
\begin{itemize}
	\item All the $identity\_inputs$ are valid known Identity Tags
	\item All the Claims in all InputSets are known Claims in a spendable state
	\item None of the Key Images are known
	\item None of the $outputs$ are in the set of known Claims
\end{itemize}
\paragraph{Effects:}
\begin{itemize}
	\item The $identity\_output$ is added to the set of known Identity Tags
	\item All the $outputs$ are added to the set of known Claims as Spendable
	\item All the Key Images are added to the set of known Key Images
\end{itemize}

\subsubsection{Redeem Request}
\label{sec:transaction:redeem_request}
\paragraph{Data:}
\begin{itemize}
	\item $input\_candidates: \mathsf{Vec\langle TransactionInputSet \rangle}$, a collection of TransactionInputSets, each of which contains a set of Claims. One of these sets is the real set being spent while the rest are decoys
	
	\item $identity\_inputs: \mathsf{Vec\langle Identity\_Tag\rangle}$, a set of identity tags. One of which is owned by the signer

	
	\item $redeem\_output: \mathsf{OpenedClaim}$, a new Opened Claims which will be destroyed if the redeem is successful
	
	\item $change\_output: \mathsf{Claim}$, a newly created Claim
	
	\item $identity\_output: \mathsf{IdentityTag}$, a single newly created Identity Tag
	
	\item $key\_images: \mathsf{Vec\langle KeyImage \rangle}$, the set of key images ensuring spent Claims cannot be spent again
	
	\item $Z: \mathsf{EdwardsPoint}$, used to store the randomness required to hide which of the input sum equals the output sum. Without this randomness, all anonymity will be lost
	
	\item $\alpha: \mathsf{Proof}$, a proof that $Z$ is a Pedersen commitment to 0. It also implies that $pZ$ is also a commitment to 0 where $p$ is any scalar
	
	\item $range\_proof: \mathsf{BulletRangeProof}$, proof that all the transaction output(s) contain commitments to values represented by a maximum number of bits. This is to stop the sum from rolling over to create money out of nothing
	
	\item $encrypted\_sender: VerifiableEncryptionOfSignerKey$, this is an encryption of the signer key that can be proven to be correct by any observer
	
	\item $bank\_data: \mathsf{Vec\langle u8 \rangle}$, this contains the encrypted bank account and routing number
\end{itemize}

\paragraph{Preconditions:}
\begin{itemize}
	\item All the $identity\_inputs$ are valid known Identity Tags
	\item $correlation\_id$ has not been used before
	\item All the Claims in all InputSets are known Claims in a spendable state
	\item None of the Key Images are in the set of known Key Images
	\item None of the $outputs$ are in the set of known Claims
\end{itemize}
\paragraph{Effects:}
\begin{itemize}
	\item The $identity\_output$ is added to the set of known Identity Tags
	\item A pending Redemption is recorded with the $correlation\_id$
	\item $redeem\_output$ is added to the set of known Claims as Pending
	\item $change\_output$ is added to the set of known Claims as Spendable
	\item The amount of the $redeem\_outputs$ is added to the total amount redeemed
\end{itemize}

\subsubsection{Exiting Redeem Request}
\label{sec:transaction:exiting_redemption}
\paragraph{Data:}
\begin{itemize}
	\item $input\_candidates: \mathsf{Vec\langle TransactionInputSet \rangle}$, a collection of TransactionInputSets, each of which contains a set of Claims. One of these sets is the real set being spent while the rest are decoys
	
	\item $redeem\_output: \mathsf{OpenedClaim}$, a new Opened Claims which will be destroyed if the Redeem is successful
	
	\item $key\_images: \mathsf{Vec\langle KeyImage \rangle}$, the set of key images ensuring spent Claims cannot be spent again
	
	\item $public\_key: Permanent Public Key$, the issuing Member's Permanent Public Key 
	
	\item $Z: \mathsf{EdwardsPoint}$, used to store the randomness required to hide which of the input sum equals the output sum. Without this randomness, all anonymity will be lost
	
	\item $\alpha: \mathsf{Proof}$, a proof that $Z$ is a Pedersen commitment to 0. It also implies that $pZ$ is also a commitment to 0 where $p$ is any scalar
	
	\item $bank\_data: \mathsf{Vec\langle u8 \rangle}$, this contains the encrypted bank account and routing number
\end{itemize}

\paragraph{Preconditions:}
\begin{itemize}
	\item The public key belongs to a Member in the Exiting state
	\item $correlation\_id$ has not been used before
	\item All the Claims in all InputSets are known Claims in a spendable state
	\item None of the Key Images are in the set of known Key Images
	\item None of the $outputs$ are in the set of known Claims
\end{itemize}
\paragraph{Effects:}
\begin{itemize}
	\item A pending Redemption is recorded with the $correlation\_id$
	\item $redeem\_output$ is added to the set of known Claims as Pending
	\item The amount of the $redeem\_outputs$ is added to the total amount redeemed
\end{itemize}

\subsubsection{Redeem Confirmation}
\label{sec:transaction:redeem_confirmation}
\paragraph{Data:}
\begin{itemize}
	\item $correlation\_id: \mathsf{[u8; 16]}$, the correlation ID of a previous redemption
	\item $\beta:\mathsf{Signature}$, a regular Schnorr signature by the Trustee
\end{itemize}
\paragraph{Preconditions:}
\begin{itemize}
	\item $correlation\_id$ refers to a currently pending Redemption
\end{itemize}
\paragraph{Effects:}
\begin{itemize}
	\item The pending redemption with $correlation\_id$ is removed
	\item $redeem\_output$ from the pending Redemption is removed from the set of known Claims
\end{itemize}

\subsubsection{Redeem Cancellation}
\label{sec:transaction:redeem_cancellation}
\paragraph{Data:}
\begin{itemize}
	\item $correlation\_id: \mathsf{[u8; 16]}$, the correlation ID of a pending redemption
	\item $\beta:\mathsf{Signature}$, a regular Schnorr signature by the Trustee
\end{itemize}
\paragraph{Preconditions:}
\begin{itemize}
	\item $correlation\_id$ refers to a currently pending Redemption
\end{itemize}
\paragraph{Effects:}
\begin{itemize}
	\item The pending Redemption with $correlation\_id$ is removed
	\item $redeem\_output$ from the pending Redemption is set to be spendable 
	\item The amount of the $redeem\_outputs$ is subtracted from the total amount redeemed
\end{itemize}
