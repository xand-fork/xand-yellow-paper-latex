\section{Executive Summary of the Confidentiality Design}

\subsection{Motivation}

Xand Networks each use their own blockchain as a ledger to record financial transactions. 
Due to the nature of most blockchains, all participants are privy to every transaction. 
Most businesses would prefer not to have all their transactions be public knowledge. 
Some blockchains, such as Bitcoin, allow participants to use a different key for every transaction as an identity protection strategy. 
However, because only Members can transact on Xand and the Members are all known to each other, their identity can't be completely hidden. 
This paper describes our private transaction system that allows Members to transact while preserving their private information and enforcing the governance requirements of the Xand Network.


\subsection{Relevant Transactions}
There are three Xand Network actions that privacy applies to:
\begin{itemize}
	\item Creation - Creation allows Members to create new Xand Claims by transferring funds into a bank account owned by the Trust
	\item Send - Sends allow Members to send Xand Claims that they own to another Member
	\item Redemption - Redemption allows a participant who holds Xand Claims to destroy those Claims and the Trustee will then transfer funds from the Trust into a specified bank account
\end{itemize}

\subsection{Requirements}
Every financial transaction on Xand has a signer and a counterparty. All other parties on the network are simply observers. For Creation and Redemption, the counterparty is the Trustee, while for Sends, the counterparty is the Member being paid. For a table summarizing the privacy of each transaction, please see \hyperref[Appendix A]{Appendix A}.

\begin{itemize}
	\item For all transactions the signer is hidden from observers but revealed to the counterparty
	\item For Send the amount is hidden from observers but revealed to the counterparty
	\item For Creation and Redemption the amount is public knowledge such that any observer can know how many Claims exist
	\item For Creation and Redemption the bank account information is hidden from observers but revealed to the counterparty
	\item Only Members are allowed to transact, but their identity is not revealed by proving their Membership
	\item Members can be added and removed from the network. Removed Members are not allowed to transact
	\item In a Send the recipient of the Claims is hidden from observers but revealed to the counterparty
	\item In a Send the sum of inputs always equals the sum of outputs
	\item All expected transactional invariants can be verified by any observer including the validators. These include:
	\begin{itemize}
		\item The Claims being consumed are owned by the signer
		\item The Claims being consumed have not been spent previously
		\item The inputs and outputs in a Send balance
		\item The amounts of creation and redemption are correct
	\end{itemize}
\end{itemize}

\subsection{Approach}

In order to fulfill the above requirements, our system adopts a number of cryptographic techniques. They will be presented here in brief and explained in depth later in the paper.

\subsubsection{Claim Ownership} In order to protect the identity of the owner of Claims, every Claim is addressed to a One Time Key. This One Time Key has a mathematical relationship to the owner's private key such that the owner can always identify Claims sent to them.

\subsubsection{Membership Proof} In order for Members to prove their membership without revealing their identity, they use a tokenized version of membership that we call Identity Tags. These are created on every new transaction to form an ever expanding pool. As observers cannot link them to Members it is necessary to include an additional proof that the Identity Tag does not belong to a removed Member.
 
\subsubsection{Claims Amount} Claims amounts are hidden using a homomorphic encryption scheme that allows addition and subtraction. This combined with Bulletproofs to prevent overflow issues allows observers to ensure that the sum of a Send's inputs is non-negative, and always equals the sum of the outputs. When doing Creations and Redemptions, the signer ``opens'' the Claims by revealing the hidden values -- allowing observers to verify that the amounts involved are correct.

\subsubsection{Signer Identity} Signer identity is partially protected by the above measures, but more is needed to prevent an observer from linking everything back to the signer's original key. Inputs to every transaction are mixed in with decoy inputs. The true input is verified using a zero-knowledge proof while not revealing which input is true. This is an adaptation of the Monero Ring Signature. As the recipient is expected to know the signer, the signer includes a proof of their identity that only the recipient can decipher.

\subsubsection{Bank Data} Bank account and routing numbers are hidden using a deterministic shared key encryption scheme. This allows the counterparty to decrypt the data, as well as the original signer.
